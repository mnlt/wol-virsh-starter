!/bin/bash

# listen to udp port 9 for packets, check if it is a magic packet
netcat -dknu -l 9 | stdbuf -o0 xxd -c 6 -p | stdbuf -o0 uniq | stdbuf -o0 grep -v 'ffffffffffff' | while read
do
    echo "Got triggered with $REPLY"
    mac="${REPLY:0:2}:${REPLY:2:2}:${REPLY:4:2}:${REPLY:6:2}:${REPLY:8:2}:${REPLY:10:2}"

    # loop through libvirt machines
    for vm in $(virsh list --all --name)
    do
        # Get the MAC address and compare with the magic packet
        vmmac=$(virsh dumpxml $vm | grep "mac address" | awk -F\' '{ print $2}')
        if [ $vmmac = $mac ]
        then
            state=$(virsh list --all|awk -v vm=$vm '{ if ($2 == vm ) print $3 }')
            echo "Found $vm with $mac in $state state"

            # Dependent on the state, resume or start
            [ $state == "пауза" ] && virsh -q resume $vm && virsh domtime --domain $vm --now
            [ $state == "выключен" ] && virsh -q start $vm
        fi
    done
done